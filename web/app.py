from flask import Flask, render_template, request
import os

app = Flask(__name__)

#The index home page
@app.route('/')
def index():
    name = request.args.get("name")
    if name == None:
        name = "this is the index"
    return render_template("index.html",name=name)

#Route HTML page
@app.route('/index/<name>')
def route(name):
    if name.endswith(".html") or name.endswith(".css"):
# If url ends with .html or .css
            dirpath = "./index/"+name
            # Should be the file name like "name.html"
            if name.startswith('~') or name.startswith(".."):
                return render_template("403.html") # Denied access
            elif os.path.isfile(dirpath):
            # Reads the file, closes it, then returns the template + file
                file = open(dirpath,"r")
                words = file.read()
                file.close()
                return render_template("okstatus.html"),words
                # Send the html/css contents
            else:
                return render_template("404.html")
    #This is if it's not a html or css file
    name = request.args.get("name")
    if name == None:
        name = "Try enter a file name"
    return name

@app.errorhandler(404)
def error404(name):
    #Exists to catch errors that route doesn't catch
    return render_template("404.html")
    #return templates with error codes
    
@app.errorhandler(403)
def error403(name):
    return return_template("403.html")

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
